import React, { useState } from "react";
import "antd/dist/antd.css";
import "./index.css";
import { Table, Tag, Button } from "antd";
import ModalPop from "./ModalPop";
import dataGen from "./data";

const TableComp = (props) => {
  const [baseData, setData] = useState(dataGen());
  let runClickComplete = (rowItem) => {
    rowItem.status = "done";
    updateData(rowItem);
  };

  let runClickUndo = (rowItem) => {
    rowItem.status = "new";
    updateData(rowItem);
  };

  let updateData = (rowItem) => {
    let temp = [...baseData];
    let index = temp.findIndex((x) => x.key === rowItem.key);
    temp.splice(index, 1, rowItem);
    setData(temp);
  };
  const columns = [
    {
      title: "Change Request Number",
      dataIndex: "orderName",
      key: "orderName",
      render: (text, row) => (
        <ModalPop text={text} request={row.orderName}></ModalPop>
      ),
    },
    {
      title: "Dealer Name",
      dataIndex: "dName",
      key: "dName",
    },
    {
      title: "Contact",
      dataIndex: "contact",
      key: "contact",
    },
    {
      title: "Status",
      key: "status",
      dataIndex: "status",
      sorter: (a, b) => {
        var statusA = a.status.toUpperCase(); // ignore upper and lowercase
        var statusB = b.status.toUpperCase(); // ignore upper and lowercase
        if (statusA < statusB) {
          return -1;
        }

        if (statusA > statusB) {
          return 1;
        }

        return 0;
      },
      render: (tag) => {
        let color = tag === "done" ? "green" : "volcano";
        return (
          <Tag color={color} key={tag}>
            {tag.toUpperCase()}
          </Tag>
        );
      },
      filters: [
        {
          text: "Done",
          value: "done",
        },
        {
          text: "New",
          value: "new",
        },
      ],
      onFilter: (value, record) => record.status.indexOf(value) === 0,
    },
    {
      title: "Action",
      key: "action",
      render: (row) => {
        var toRender =
          row.status === "new" ? (
            <Button
              type="primary"
              shape="round"
              size="default"
              onClick={() => runClickComplete(row)}
            >
              Complete
            </Button>
          ) : (
            <Button
              style={{
                background: "#D33348",
                borderColor: "white",
                color: "white",
              }}
              shape="round"
              size="default"
              onClick={() => runClickUndo(row)}
            >
              Undo
            </Button>
          );

        return toRender;
      },
    },
  ];

  return <Table columns={columns} dataSource={baseData} />;
};

export default TableComp;
