/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState } from "react";
import "antd/dist/antd.css";
import "./index.css";
import { Modal } from "antd";

const ModalPop = (props) => {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = (e) => {
    e.preventDefault();
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return (
    <>
      <a onClick={showModal}>{props.text}</a>
      <Modal
        title={props.request}
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <h3>More request #{props.text} info here...</h3>
      </Modal>
    </>
  );
};

export default ModalPop;
