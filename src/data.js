function getRndInteger(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

class RequestOrder {
  constructor(key, orderName, dName, contact, status) {
    this.key = key;
    this.orderName = orderName;
    this.dName = dName;
    this.contact = contact;
    this.status = status;
  }
}

function rngString(length) {
  var result = "";
  var characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

let dataGen = () => {
  let count = getRndInteger(10, 35);
  let dataToReturn = [];
  for (let i = 1; i <= count; i++) {
    let email = rngString(7) + "@gmail.com";
    let dName = rngString(7) + " Dealer";
    let draw = getRndInteger(0, 2);
    let status = draw === 0 ? "new" : "done";
    let request = new RequestOrder(
      i,
      getRndInteger(10000, 1000000000),
      dName,
      email,
      status
    );
    dataToReturn.push(request);
  }

  return dataToReturn;
};

export default dataGen;
