import React from "react";
import ReactDOM from "react-dom";
import "antd/dist/antd.css";
import "./index.css";
import TableComp from "./TableComp";

ReactDOM.render(
  <>
    <TableComp />
  </>,
  document.getElementById("container")
);
